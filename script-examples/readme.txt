Do not directly modify the scripts.

Instead, copy the file as [some filename].test.shlang.

Files following the pattern of *.test.shlang get ignored by git.
