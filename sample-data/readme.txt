Do not directly modify these CSVs.

Instead, copy the file as [some filename].test.csv.

Files following the pattern of *.test.csv get ignored by git.
