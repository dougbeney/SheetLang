import sys
import readline
import argparse
import parser as parser
import language as language

try:
	cli_tool = argparse.ArgumentParser(description='Process some integers.')
	cli_tool.add_argument('scriptfile', nargs='?', help='Filename of your script.')
	args = cli_tool.parse_args()
	if args.scriptfile is not None:
		# Load from a file
		with open(args.scriptfile) as f:
			lines = f.read().splitlines()
			for line in lines:
				command_object = parser.GetCommandObject(line)
				if command_object:
					language.RunLine(command_object)
	else:
		# Enter the REPL
		while True:
			line = input('|>')
			if parser.StringContainsOneOf(line, ["quit", "q", "exit"]):
				break
			# Process The Line
			command_object = parser.GetCommandObject(line)
			if command_object:
				language.RunLine(command_object)
except KeyboardInterrupt:
	print("")
	sys.exit()
