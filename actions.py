from .sheetops import Sheet

MODE_ROWS = 0
MODE_COLS = 1

def delete_func(command_object):
	mode = None
	min_args = 6
	location = ""
	cmdobj_len = len(command_object)

	if cmdobj_len >= 6:
		rowsorcols = command_object[1]
		if rowsorcols == "rows":
			mode = MODE_ROWS
		elif rowsorcols == "cols":
			mode = MODE_COLS
		else:
			print("You must specify rows or columns when deleting")

		for i in range(2, cmdobj_len):
			arg = command_object[i]
			if str(arg).lower() == "where":
				location = command_object[i+1]        # ex. 'f' <- meaning column-f
				condition = command_object[i+2]       # ex. 'contains'
				condition_value = command_object[i+3] # ex. 'ubuntu.com'

				# The delete operation
				sheet = Sheet("")
				if mode is MODE_ROWS:
					pass
				else:
					pass

				# i = i+4 if that doesn't go outside the array's bounds
				i = i+4 if i+4 < cmdobj_len else cmdobj_len
			i = i + 1
	else:
		print("Delete takes a minimum of 6 arguments. You gave", cmdobj_len)

list = [
	{
		"name": "delete",
		"func": delete_func
	}
]
