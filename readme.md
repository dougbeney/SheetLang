SheetLang
---------

User-friendly SQL-like language for Spreadsheets

Being created in Python for rapid development. Might be ported to C for better performance after the minimal viable product is created.


## Sample Data & Scripts

Sample data is found in the sample-data/ folder and sample scripts are in the sample-scripts/ folder.

Do not directly modify the CSVs.

Instead, copy the file as [some filename].test.csv.

Files following the pattern of *.test.csv get ignored by git.

Same goes for scripts. If you create your own test script or want to modify an existing one use the pattern *.test.shlang as it will be ignored by git.
