#### MODES #####
MODE         = 0 # Search mode by default
################
MODE_SEARCH  = 0
MODE_COMMAND = 1
MODE_STRING  = 2
MODE_NUMBER  = 3
################

def StringContainsOneOf(var, selection):
	for i in selection:
		if var == i:
			return True
	return False

def ModeToString(mode):
	if MODE   is MODE_SEARCH:
		return "search"
	elif MODE is MODE_COMMAND:
		return "command"
	elif MODE is MODE_STRING:
		return "string"
	elif MODE is MODE_NUMBER:
		return "number"
	return "unknown"

def GetCommandObject(line):
	command_object = []
	# for char in line
	working_string = None
	for index, c in enumerate(line):
		global MODE
		# Set proper mode if search mode
		if MODE is MODE_SEARCH:
			if c.isalpha():
				MODE = MODE_COMMAND
			else:
				if c == '"' or c == "'":
					MODE = MODE_STRING
					continue
				else:
					MODE = MODE_NUMBER

		def ShouldFireOff(index, strlength, char, escapechar):
			boolarray = [
				False, # At the last index in array
				False  # Hit an escape character and is ready to go 
			]
			if index is strlength-1:
				boolarray[0] = True
				boolarray[1] = True
			if char is escapechar:
				boolarray[1] = True
			return boolarray

		# Command-Specific processing
		# sfo stands for should-fire-off
		sfo_space = ShouldFireOff(index, len(line), c, " ")
		sfo_singlequote = ShouldFireOff(index, len(line), c, "'")
		sfo_doublequote = ShouldFireOff(index, len(line), c, '"')
		if sfo_space[0] or sfo_singlequote[0] or sfo_doublequote[0]:
			if working_string:
				if c is not '"' and c is not "'" and c is not " ":
					working_string += c

		# # # # # # # # # # # # # # # # # # # # # # #
		#START: THIS IS WHERE THE COMMMANDS GET SENT#
		# # # # # # # # # # # # # # # # # # # # # # #

		if   MODE is MODE_COMMAND: # COMMAND MODE
			if sfo_space[1]:
				MODE = MODE_SEARCH
				command_object.append(working_string)
				working_string = None

		elif MODE is MODE_STRING: # STRING MODE
			if sfo_singlequote[1] or sfo_doublequote[1]:
				MODE = MODE_SEARCH
				command_object.append(working_string)
				working_string = None

		elif MODE is MODE_NUMBER: # NUMBER MODE
			if sfo_space[1]:
				if working_string is not None:
					if(working_string.isdigit()):
						command_object.append(working_string)
					else:
						print(working_string, "is not a proper number.")
				MODE = MODE_SEARCH
				working_string = None

		# # # # # # # # # # # # # # # # # # # # # # #
		#END: THIS IS WHERE THE COMMMANDS GET SENT  #
		# # # # # # # # # # # # # # # # # # # # # # #

		# add to working string
		if MODE is not MODE_SEARCH:
			if working_string is None:
				working_string = ""
			working_string += c
	if len(command_object) > 0:
		return command_object
	else:
		return False

